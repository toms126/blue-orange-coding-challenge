package com.matterdor.server.services;

import com.matterdor.server.entities.User;

public interface IUserService {

    User findByUsername(String username);

    User findById(Long userId);

    User create(User u) throws Exception;

    void updateRole(User u, String role);

    void updatePassword(User user, String old_password, String new_password) throws Exception;

    void updatePassword(User user, String new_password);

    void delete(Long userId);

    void updateFirstname(User user, String firstname);

    void updateLastname(User user, String lastname);

    void updateLockState(User user, Boolean lock);

    void updateEnabledState(User user, Boolean enabled);

}

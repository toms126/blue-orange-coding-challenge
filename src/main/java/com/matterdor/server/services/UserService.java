package com.matterdor.server.services;

import com.matterdor.server.config.CommandLineAppStartupRunner;
import com.matterdor.server.dao.RoleRepository;
import com.matterdor.server.dao.UserRepository;
import com.matterdor.server.entities.Role;
import com.matterdor.server.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

@Service
public class UserService implements IUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private UserRepository userRepo;

    private PasswordEncoder passwordEncoder;

    private RoleRepository roleRepo;

    @Autowired
    public UserService(
            UserRepository userRepo,
            PasswordEncoder passwordEncoder,
            RoleRepository roleRepo) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
        this.roleRepo = roleRepo;
    }

    public User findByUsername(String username){
        return this.userRepo.findByUsername(username);
    }

    public User findById(Long userId){
        Optional<User> value = this.userRepo.findById(userId);
        return value.orElseGet(() -> this.userRepo.findById(userId).get());
    }

    public User create(User u) throws Exception {
        User user = this.findByUsername(u.getUsername());
        if (user == null){
            u.setPassword(this.passwordEncoder.encode(u.getPassword()));
            u = this.userRepo.save(u);
        } else {
            throw new Exception("Username already exists");
        }
        LOGGER.info("User created");
        return u;
    }

    public void updateRole(User u, String role){
        Role targetUserRole = u.getRoles().get(0);
        targetUserRole.setName(role);
        this.roleRepo.save(targetUserRole);
        LOGGER.info("Role Updated");
    }

    public void updatePassword(User user, String old_password, String new_password) throws Exception {
        String currentPasswordHash = user.getPassword();
        Boolean match = this.passwordEncoder.matches(old_password, currentPasswordHash);
        if (!match){
            LOGGER.info("Old password do not match");
            throw new Exception("Old password does not match the new password");
        } else {
            user.setPassword(this.passwordEncoder.encode(new_password));
            this.userRepo.save(user);
            LOGGER.info("Password updated");
        }
    }

    public void updatePassword(User user, String new_password) {
        user.setPassword(this.passwordEncoder.encode(new_password));
        this.userRepo.save(user);
        LOGGER.info("Admin password updated");
    }

    public void delete(Long userId){
        this.userRepo.delete(this.findById(userId));
        LOGGER.info("User deleted");
    }

    public void updateFirstname(User user, String firstname){
        user.setFirstname(firstname);
        this.userRepo.save(user);
        LOGGER.info("User firstname updated");
    }

    public void updateLastname(User user, String lastname){
        user.setLastname(lastname);
        this.userRepo.save(user);
        LOGGER.info("User lastname updated");
    }

    public void updateLockState(User user, Boolean lock){
        user.setLocked(lock);
        this.userRepo.save(user);
        LOGGER.info("User Lock updated");
    }

    public void updateEnabledState(User user, Boolean enabled){
        user.setEnabled(enabled);
        this.userRepo.save(user);
    }

}

package com.matterdor.server.config;

import com.matterdor.server.entities.Role;
import com.matterdor.server.entities.User;
import com.matterdor.server.services.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandLineAppStartupRunner.class);

    @Autowired
    IUserService userService;

    @Override
    public void run(String...args) {
        User u = this.userService.findByUsername("admin");
        if (u == null){
            try {
                u = new User(
                        "admin",
                        "admin",
                        "admin",
                        "admin",
                        true,
                        false,
                        new Date(),
                        new Date(),
                        false,
                        Arrays.asList(new Role("Admin"))
                );
                u = this.userService.create(u);
            } catch (Exception e) {
                LOGGER.error("Failed to create an admin user");
            }
        }
    }

}

package com.matterdor.server.dao;

import com.matterdor.server.entities.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
}

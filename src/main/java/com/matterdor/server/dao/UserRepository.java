package com.matterdor.server.dao;

import com.matterdor.server.entities.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);

    User findByUsernameAndPassword(String username, String password);

    Long countAllByUsernameContainingIgnoreCaseOrFirstnameContainingIgnoreCaseOrLastnameContainingIgnoreCaseOrderByCreated(String username, String firstname, String lastname);

    User findTopByLastResetBefore(Date query);

}

package com.matterdor.server;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.matterdor.server.dao.RoleRepository;
import com.matterdor.server.dao.UserRepository;
import com.matterdor.server.entities.Role;
import com.matterdor.server.entities.User;
import com.matterdor.server.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource("classpath:test.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserServiceDAOTests {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RoleRepository roleRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private UserService userService;

    private ListAppender<ILoggingEvent> listAppender;

    private List<ILoggingEvent> logsList;

    @BeforeEach
    public void testSetUp() {
        userService = new UserService(userRepo,passwordEncoder,roleRepo);
        assertNotNull(userService);

        Logger UserServiceLogger = (Logger) LoggerFactory.getLogger(UserService.class);
        listAppender = new ListAppender<>();
        listAppender.start();
        UserServiceLogger.addAppender(listAppender);
        logsList = listAppender.list;
    }

    @Test
    public void findCreateDeleteTest() throws Exception {
        User user = new User();
        user.setUsername("username");
        user.setPassword("oldPassword");

        //find user
        assertNull(userService.findByUsername("username"));

        //create user
        userService.create(user);
        assertEquals("User created", logsList.get(0)
                .getMessage());
        assertEquals("username",userService.findByUsername("username").getUsername());

        //delete user
        Long Id = user.getId();
        userService.delete(Id);
        assertEquals("User deleted", logsList.get(1)
                .getMessage());
        assertNull(userService.findByUsername("username"));
    }

    @Test
    public void findCreateDeleteMultipleUsersTest() throws Exception {
        User user = new User();
        user.setUsername("username");
        user.setPassword("oldPassword");

        User user1 = new User();
        user1.setUsername("username1");
        user1.setPassword("oldPassword1");

        //find user
        assertNull(userService.findByUsername("username"));
        assertNull(userService.findByUsername("username1"));
        assertNull(userService.findByUsername("username2"));

        //create user
        userService.create(user);
        assertEquals("username",userService.findById(user.getId()).getUsername());
        assertThrows(NoSuchElementException.class, () -> userService.findById(101L));
        userService.create(user1);
        assertEquals("username1",userService.findById(user1.getId()).getUsername());
        assertNotNull(userService.findByUsername("username1"));

        //delete user
        userService.delete(user.getId());
        assertNull(userService.findByUsername("username"));
        assertNotNull(userService.findByUsername("username1"));

        //delete user 1
        userService.delete(user1.getId());
        assertNull(userService.findByUsername("username1"));
    }

    @Test
    public void createUpdateTest() throws Exception {
        User user = new User();
        user.setUsername("username");
        user.setId(1L);
        user.setPassword("oldPassword");
        List<Role> roleList = new ArrayList<>();
        Role role = new Role();
        roleList.add(role);
        user.setRoles(roleList);
        userService.create(user);
        assertEquals("User created", logsList.get(0)
                .getMessage());

        //test update role
        userService.updateRole(user,"manager");
        assertEquals("Role Updated", logsList.get(1)
                .getMessage());
        assertEquals("manager",user.getRoles().get(0).getName());

        //update password
        String oldPasswordHash = user.getPassword();
        userService.updatePassword(user, "oldPassword", "newPassword");
        assertEquals("Password updated", logsList.get(2)
                .getMessage());
        assertNotEquals(oldPasswordHash, user.getPassword());

        //update admin password
        oldPasswordHash = user.getPassword();
        userService.updatePassword(user,"newPassword01");
        assertEquals("Admin password updated", logsList.get(3)
                .getMessage());
        assertNotEquals(oldPasswordHash, user.getPassword());

        //update firstname
        userService.updateFirstname(user, "uncommonFirstname");
        assertEquals("User firstname updated", logsList.get(4)
                .getMessage());
        assertEquals("uncommonFirstname",user.getFirstname());

        //update lastname
        userService.updateLastname(user, "uncommonLastname");
        assertEquals("User lastname updated", logsList.get(5)
                .getMessage());
        assertEquals("uncommonLastname",user.getLastname());

        //update lock state
        userService.updateLockState(user,false);
        assertEquals(false,user.getLocked());
        userService.updateLockState(user,true);
        assertEquals(true,user.getLocked());

        //update enabled state
        userService.updateEnabledState(user,false);
        assertEquals(false,user.getEnabled());
        userService.updateEnabledState(user,true);
        assertEquals(true,user.getEnabled());
    }

}

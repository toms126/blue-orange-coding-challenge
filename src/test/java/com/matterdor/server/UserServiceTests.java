package com.matterdor.server;

import com.matterdor.server.dao.RoleRepository;
import com.matterdor.server.dao.UserRepository;
import com.matterdor.server.entities.Role;
import com.matterdor.server.entities.User;
import com.matterdor.server.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource("classpath:test.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserServiceTests {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RoleRepository roleRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private UserService userService;

    private ListAppender<ILoggingEvent> listAppender;

    private List<ILoggingEvent> logsList;

    @BeforeEach
    public void constructorTest() {
        userService = new UserService(userRepo,passwordEncoder,roleRepo);
        assertNotNull(userService);

        //setup list appender for logger
        Logger UserServiceLogger = (Logger) LoggerFactory.getLogger(UserService.class);
        listAppender = new ListAppender<>();
        listAppender.start();
        UserServiceLogger.addAppender(listAppender);
        logsList = listAppender.list;
    }

    @Test
    public void findUserByNameTest() {
        User user = Mockito.mock(User.class);
        when(userRepo.findByUsername("username")).thenReturn(user);
        assertEquals(user,userService.findByUsername("username"));
        verify(userRepo).findByUsername("username");
    }

    @Test
    public void findUserByIdTest() {
        User user = Mockito.mock(User.class);
        when(userRepo.findById(1L)).thenReturn(java.util.Optional.ofNullable(user));
        assertEquals(user,userService.findById(1L));
        verify(userRepo).findById(1L);
    }

    @Test
    public void createNullTest() throws Exception {
        User user = Mockito.mock(User.class);
        when(userRepo.findByUsername("username")).thenReturn(null);
        when(user.getPassword()).thenReturn("password");
        when(passwordEncoder.encode("password")).thenReturn("encodedPassword");
        userService.create(user);
        verify(userRepo).save(user);
        verify(passwordEncoder).encode("password");
    }

    @Test
    public void createExistedTest() {
        User user = Mockito.mock(User.class);
        when(user.getUsername()).thenReturn("username");
        when(userRepo.findByUsername("username")).thenReturn(user);
        assertNotNull(userService.findByUsername("username"));
        Exception exception = assertThrows(Exception.class, () -> {
            userService.create(user);
        });
        String expectedMessage = "Username already exists";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    public void createSuccessTest() throws Exception {
        User user = Mockito.mock(User.class);
        when(userRepo.findByUsername("username")).thenReturn(user);
        userService.create(user);

        assertEquals("User created", logsList.get(0)
                .getMessage());
        assertEquals(Level.INFO, logsList.get(0)
                .getLevel());
    }

    @Test
    public void updateRoleTest() {
        User user = Mockito.mock(User.class);
        Role role = Mockito.mock(Role.class);
        List<Role> roleList = new ArrayList<>();
        roleList.add(role);
        when(user.getRoles()).thenReturn(roleList);
        userService.updateRole(user,"manager");
        verify(roleRepo).save(role);
        verify(role).setName("manager");

        assertEquals("Role Updated", logsList.get(0)
                .getMessage());
        assertEquals(Level.INFO, logsList.get(0)
                .getLevel());
    }


    @Test
    public void updatePasswordFailTest() {
        User user = Mockito.mock(User.class);
        when(user.getPassword()).thenReturn("password");
        when(passwordEncoder.matches("differentpassword","password")).thenReturn(false);

        Exception exception = assertThrows(Exception.class, () -> {
            userService.updatePassword(user,"differentpassword", "newpassword");
        });
        String expectedMessage = "Old password does not match the new password";
        String actualMessage = exception.getMessage();
        assertEquals(actualMessage, expectedMessage);

        assertEquals("Old password do not match", logsList.get(0)
                .getMessage());
        assertEquals(Level.INFO, logsList.get(0)
                .getLevel());
    }

    @Test
    public void updatePasswordSuccessTest() throws Exception {
        User user = Mockito.mock(User.class);
        when(user.getPassword()).thenReturn("password");
        when(passwordEncoder.matches("password","password")).thenReturn(true);
        when(passwordEncoder.encode("newpassword")).thenReturn("encodednewpassword");

        userService.updatePassword(user,"password", "newpassword");
        verify(user).setPassword("encodednewpassword");
        verify(passwordEncoder).encode("newpassword");
        verify(userRepo).save(user);

        assertEquals("Password updated", logsList.get(0)
                .getMessage());
        assertEquals(Level.INFO, logsList.get(0)
                .getLevel());
    }

    @Test
    public void updatePasswordWithoutCheckingOldPasswordTest() {
        User user = Mockito.mock(User.class);
        when(passwordEncoder.encode("newpassword")).thenReturn("encodednewpassword");

        userService.updatePassword(user, "newpassword");
        verify(user).setPassword("encodednewpassword");
        verify(passwordEncoder).encode("newpassword");
        verify(userRepo).save(user);

        assertEquals("Admin password updated", logsList.get(0)
                .getMessage());
        assertEquals(Level.INFO, logsList.get(0)
                .getLevel());
    }

    @Test
    public void deleteUserByIdTest() {
        User user = Mockito.mock(User.class);
        when(userRepo.findById(1L)).thenReturn(java.util.Optional.ofNullable(user));
        assertEquals(user,userService.findById(1L));

        userService.delete(1L);
        verify(userRepo).delete(user);

        assertEquals("User deleted", logsList.get(0)
                .getMessage());
        assertEquals(Level.INFO, logsList.get(0)
                .getLevel());
    }

    @Test
    public void updateFirstnameTest() {
        User user = Mockito.mock(User.class);
        userService.updateFirstname(user,"James");
        verify(user).setFirstname("James");
        verify(userRepo).save(user);
        assertEquals("User firstname updated", logsList.get(0)
                .getMessage());
        assertEquals(Level.INFO, logsList.get(0)
                .getLevel());
    }

    @Test
    public void updateLastnameTest() {
        User user = Mockito.mock(User.class);
        userService.updateLastname(user,"Smith");
        verify(user).setLastname("Smith");
        verify(userRepo).save(user);
        assertEquals("User lastname updated", logsList.get(0)
                .getMessage());
        assertEquals(Level.INFO, logsList.get(0)
                .getLevel());
    }

    @Test
    public void updateLockStateTest() {
        User user = Mockito.mock(User.class);
        userService.updateLockState(user,false);
        verify(user).setLocked(false);
        verify(userRepo).save(user);
        assertEquals("User Lock updated", logsList.get(0)
                .getMessage());
        assertEquals(Level.INFO, logsList.get(0)
                .getLevel());
    }

    @Test
    public void updateLockStateTest1() {
        User user = Mockito.mock(User.class);
        userService.updateLockState(user,true);
        verify(user).setLocked(true);
        verify(userRepo).save(user);
        assertEquals("User Lock updated", logsList.get(0)
                .getMessage());
        assertEquals(Level.INFO, logsList.get(0)
                .getLevel());
    }

    @Test
    public void updateEnabledTest() {
        User user = Mockito.mock(User.class);
        userService.updateEnabledState(user,false);
        verify(user).setEnabled(false);
        verify(userRepo).save(user);

        userService.updateEnabledState(user,true);
        verify(user).setEnabled(true);
        verify(userRepo,times(2)).save(user);
    }

}


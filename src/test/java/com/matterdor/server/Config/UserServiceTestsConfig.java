package com.matterdor.server.Config;

import com.matterdor.server.dao.RoleRepository;
import com.matterdor.server.dao.UserRepository;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;

@Profile("test")
@Configuration
public class UserServiceTestsConfig {

    @Bean
    @Primary
    public UserRepository getUserRepositoryMock() {
        return Mockito.mock(UserRepository.class);
    }

    @Bean
    @Primary
    public RoleRepository getRoleRepositoryMock() {
        return Mockito.mock(RoleRepository.class);
    }

    @Bean
    @Primary
    public PasswordEncoder getPasswordEncoder() {
        return Mockito.mock(PasswordEncoder.class);
    }

}

